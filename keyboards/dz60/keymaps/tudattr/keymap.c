/* Copyright 2021 @tudattr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H

// Layers
#define _DEFAULT 0
#define _MACRO 1
#define _FUNCTION 2
#define _LIGHTS 3

enum custom_keycodes {
    SHRG = SAFE_RANGE,
    UWU,
    LYING,
    TBL,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    /* Base layer
     *
     * ,---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------.
     * | ESC     |     1     |     2     |     3     |     4     |     5     |     6     |     7     |     8     |     9     |     0     |     -     |     =     |     \     |     ~     |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * | Tab            |     Q     |     W     |     E     |     R     |     T     |     Y     |     U     |     I     |     O     |     P     |     [     |     ]     |   Backspace    |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * | LCtrl             |     A     |     S     |     D     |     F     |     G     |     H     |     J     |     K     |     L     |     ;     |     '     |          Enter          |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * | Shift                  |     Z     |     X     |     C     |     V     |     B     |     N     |     M     |     <     |     >     |     ?     |   Shift   |      _LIGHTS       |
     * `---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------´
     *                    |   LGui   |     LAlt     |                            _FUNCTION/Space                             |     RAlt    |   PrntScrn   |  _MACRO   |
     *                    `--------------------------------------------------------------------------------------------------------------------------------------------´
     */

    [_DEFAULT] = LAYOUT_all (
            //                  2        3        4        5        6        7        8        9        10       11       12       13       14           15       16
            KC_ESC,             KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSLS,     KC_GRV,
            KC_TAB,             KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSPC,
            KC_LCTRL,           KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT, KC_ENT,
            KC_LSFT,            KC_NO,   KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT, MO(_LIGHTS), KC_NO,
            KC_NO,              KC_LGUI, KC_LALT,          KC_NO,            LT(_FUNCTION, KC_SPC),     KC_NO,   KC_RALT, KC_PSCR, KC_NO,   MO(_MACRO),  KC_NO),


    /* Macro Layer (Default Unicode: Windows (Windows, Linux))
     *
     * ,---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------.
     * |   Mute    |           |           |           |           |           |           |           |           |           |           |           |           |          |          |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * |                |           |           |           |           |           |           |           |           |           |           |           |           |                |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * |                   |           |           |           |           |           | Previous  |  VolDown  |   VolUp   |   Next    |           |           |                         |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * |                     |          | (╯°□°）╯彡 ┻━┻ | ¯\_(ツ)_/¯ |           | BrghtDown |  BrghtUp  |           |           |           |           |    uwu    |    _(:3」∠)_     |
     * `---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------´
     *                    |          |             |                                                                        |   PrvUnicode  |   NxtUnicode |           |
     *                    `--------------------------------------------------------------------------------------------------------------------------------------------´
     */

    [_MACRO] = LAYOUT_all (
            //        2         3         4         5         6         7         8         9         10        11         12        13        14        15       16
            KC_MUTE,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,
            KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS,  KC_TRNS,
            KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_MPRV,  KC_VOLD,  KC_VOLU,  KC_MNXT,  KC_TRNS,   KC_TRNS,  KC_TRNS, 
            KC_TRNS,  KC_NO, KC_TRNS,    TBL,      SHRG,     KC_TRNS,  KC_BRID,  KC_BRIU,  KC_TRNS,  KC_TRNS,  KC_TRNS,   KC_TRNS,  UWU,      LYING,    KC_NO,
            KC_NO,    KC_TRNS,  KC_TRNS,            KC_NO,              KC_TRNS,                      KC_NO,    UC_RMOD,   UC_MOD,   KC_NO,    KC_TRNS,  KC_NO),


    /* Function Layer
     *
     * ,---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------.
     * |    ESC    |    F1     |    F2     |    F3     |    F4     |    F5     |    F6     |    F7     |    F8     |    F9     |    F10    |    F11    |    F12    |    INS   |    `     |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * |                |    F13    |    F14    |    F15    |    F16    |    F17    |    F18    |    F19    |    F20    |    F21    |    F22    |    F22    |    F23    |      F24       |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * |                   |           |           |           |           |   Left    |   Down    |    Up     |   Right   |           |           |           |                         |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * |                        |           |           |           |           |           |           |           |           |           |           |           |                    |
     * `---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------´
     *                    |          |              |                                                                        |              |              |           |
     *                    `--------------------------------------------------------------------------------------------------------------------------------------------´
     */

    [_FUNCTION] = LAYOUT_all (
            //        2         3         4         5         6         7         8         9         10        11         12        13       14        15       16
            KC_ESC,   KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,    KC_F6,    KC_F7,    KC_F8,    KC_F9,    KC_F10,    KC_F11,   KC_F12,  KC_INS,   KC_GRV,
            KC_TRNS,  KC_F13,   KC_F14,   KC_F15,   KC_F16,   KC_F17,   KC_F18,   KC_F19,   KC_F20,   KC_F21,   KC_F22,    KC_F23,   KC_F24,  KC_DEL,
            KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_LEFT,  KC_DOWN,  KC_UP,    KC_RIGHT, KC_TRNS,   KC_TRNS,  KC_TRNS,
            KC_TRNS,  KC_NO,    KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_NO,
            KC_NO,    KC_TRNS,  KC_TRNS,            KC_NO,              KC_TRNS,                      KC_NO,    KC_TRNS,   KC_NO,    KC_TRNS,  KC_TRNS, KC_NO),


    /* Light Layer
     *
     * ,---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------.
     * |  Toggle   |  Static   | Breathing |  Rainbow  |  KnghtRdr | Christmas | Gradient  |   Swirl   |   Snake   |           |           |           |           |          |          |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * |                |   HueUp   |   SatUp   |  BrghtUp  |   SpdUp   |           |           |           |           |           |           |           |           |                |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * |                   |  HueDown  |  SatDown  | BrghtDown |  SpdDown  |           |           |           |           |           |           |           |     Bootloader Mode     |
     * |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
     * |                        |           |           |           |           |           |           |           |           |           |           |           |                    |
     * `---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------´
     *                    |          |              |                                                                        |              |              |           |
     *                    `--------------------------------------------------------------------------------------------------------------------------------------------´
     */

    [_LIGHTS] = LAYOUT_all (
            //        2        3        4        5         6         7       8         9         10        11       12       13       14       15       16
            RGB_TOG,  RGB_M_P, RGB_M_B, RGB_M_R, RGB_M_K,  RGB_M_X, RGB_M_G, RGB_M_SW, RGB_M_SN, KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
            KC_TRNS,  RGB_HUI, RGB_SAI, RGB_VAI, RGB_SPI,  KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
            KC_TRNS,  RGB_HUD, RGB_SAD, RGB_VAD, RGB_SPD,  KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, RESET,
            KC_TRNS,  KC_NO,   KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_NO,
            KC_NO,    KC_TRNS, KC_TRNS,          KC_NO,             KC_TRNS,                     KC_NO,    KC_TRNS, KC_NO,   KC_TRNS, KC_TRNS,   KC_NO),
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch(keycode) {
        case TBL:
            if (record->event.pressed) {
                send_unicode_string("(╯°□°）╯彡 ┻━┻");
            }
            break;

        case SHRG:
            if (record->event.pressed) {
                send_unicode_string("¯\\_(ツ)_/¯");
            }
            break;

        case UWU:
            if (record->event.pressed) {
                SEND_STRING("`MMMMMMMMM0;xKkKKKKKKKKKKKKKKKKKKKk0KKKKKKKKKKKO',N`\n");
                SEND_STRING("`MMMMMMMNoo0KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK0l;`\n");
                SEND_STRING("`MMMMMWd;OKKKKKKKKKKoKKKKKKKKKKKKO0KKKKKKKKKKKKKKKk`\n");
                SEND_STRING("`MMMMW:oKKKKKKKKKKKdlKKKKKKKKKKK0okKKKKKKKKKKKKKKKX`\n");
                SEND_STRING("`MMMN:kKKKKKKKKKKK0'dKKKKKKKKKKKc,oKKKKKKKKKKKKKKKK`\n");
                SEND_STRING("`MMX,kKKKKKK0kKKK0;.kKKKKKKKKKKK'd;0KKKKKKKKKKKKKKK`\n");
                SEND_STRING("`MW;oKKKKKKK:dKKk,k,xKKKKOKKKKKl;WdcKKKKxKKKOOKKKKK`\n");
                SEND_STRING("`Mx:KKKKKKKc:K0l;KWclKKKdlKKKKd'XWWcoKKKxlKKKlKKKKK`\n");
                SEND_STRING("`W,kKKKKKO;l0d',dodc.OKK:dKKKo;KOdoo.l0KKcdKK:0KKKK`\n");
                SEND_STRING("`0;KKKKKo;dlcoOKNWWWdcKKccK0coNWWWWNKd'lx0:dKcxKKKK`\n");
                SEND_STRING("`doKKxo:.dONWWWWWWWWWklOO,oxXWWWWWWWWWXxd0KoocoKKKK`\n");
                SEND_STRING("`lxKO;00 dWWXXWWWWWkNWWOkxcxXWW0dXWWWWWXO0O0O;oKKKK`\n");
                SEND_STRING("`lxO':KK:kWN.:WWWW0.xWWWWWWWNWWc.dWWWW0.cWWWWcdKKKK`\n");
                SEND_STRING("`ko:'dKKodWN. 'cl, .XWWWWWWWWWWO. :oo;. dWWWW'kKKKK`\n");
                SEND_STRING("`N:;,OK0o:KK0o;,,;dKOWWWNNWWWN:xXl,..'c0NNWWO,KKKKK`\n");
                SEND_STRING("`Mxd'000o'OOOOO0KXNX.dWO..kWWl ONNXKKK00KKXX;oKKKKK`\n");
                SEND_STRING("`MM0'K00,:OOOO00KXNWO.''Ok'.'.xNNXK0000000Kd,0KKKKK`\n");
                SEND_STRING("`MMo.0K0,oKKKKXXNNWWWNXWWWWXNWWWNNXKKKKKKK0,kKKKKOo`\n");
                SEND_STRING("`MMc'xKK,,XNNWWWWWWWWWWWWWWWWWWWWWNNNNNNNX:dKKKKk.,`\n");
                SEND_STRING("`MM:c:Kk.',KWWWWWWWWWWWWWWWWWWWWWWWWWWWWNlxKKKKd'oK`\n");
                SEND_STRING("`MMcdlox.:,.c0WWWWWWWWWWWWWWWWWWWWWWWWN0;oKKKk;c0KK`\n");
                SEND_STRING("`MMdoKdl.ccc:,,cox0KXNNWWWWWNNXX0kdol:'l0KOo;,xKKKK`\n");
            }
            break;

        case LYING:
            if (record->event.pressed) {
                send_unicode_string("_(:3」∠)_");
            }
            break;

        default:
            if (record->event.pressed) {

            }
            break;
    }
    return true;
}

void eeconfig_init_user(void) {
    set_unicode_input_mode(UC_WINC);
}
